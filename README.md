# Get Shaarli Bookmarks and write Asciidoctor Source for Hugo Blog

[Following script](https://gitlab.com/leyrer/shaarli2blog) - thanks [Martin](https://twitter.com/leyrer) - gave me the idea to automate my blogpost "linkdumps" from my shaarli instance.

As I'm really bad with Perl, I created it with Python.

## Install requirements

```bash
pip3 install -r requirements.txt
```

## Change properties

Edit `shaarli2hugo.ini` and replace paths and urls.

You can change the shaarli URL, path to the hugo posts, topics and tags (to search and post) and so on.

## Run

`./shaarli2hugo.py`

The scripts collects all shaarli links and descriptions of the actual week. So from Monday to the day you run it. Then it creates an Asciidoc file which is prepared for [Hugo](https://gohugo.io).

The script creates a file with name `linkdump_` + `weeknumber` + `year` + `adoc` and it will overwrite the file if already present, so it updates posts during the week and you can run the script multiple times.

## Jupyter Notebook `shaarli2hugo.ipynb`

I keep this in the repo for more tests, but it is not necessary to run the script!
