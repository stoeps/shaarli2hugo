#!/usr/bin/env bash
#
# Author:       Christoph Stoettner
# E-Mail:       christoph.stoettner@stoeps.de
#
# Description:  Create Linkdump and add to hugo repository
#
#               Add to crontab: 10 17 * * 5,6 cd PATH/shaarli2hugo; ./cronjob.sh
#
# Date:         2020-09-30
#
# (c) 2020 by Christoph Stoettner - All rights reserved
#

HOME=/home/stoeps
TIMESTAMP=$(date +"%A %d.%m.%Y %H:%M")

SCRIPTPATH="${HOME}/work/git/shaarli2hugo"

source ${HOME}/.bashrc

cd "${SCRIPTPATH}" || exit
echo "Start creating linkdump: ${TIMESTAMP}" >> cron.log

python3 ./shaarli2hugo.py

cd ../stoeps.de || exit

git add content/post/2020/linkdump_*.adoc
git commit -m "Add new linkdump at ${TIMESTAMP}" >> cron.log
sleep 10
git push origin master >> cron.log
echo . >> cron.log
