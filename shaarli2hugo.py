#!/usr/bin/env python3
# coding: utf-8
import feedparser
import datetime
import dateutil.parser as dp
import dateutil.tz as dtz
import pypandoc
import sys
import configparser
import os
import logging

from pypandoc.pandoc_download import download_pandoc
# see the documentation how to customize the installation path
# but be aware that you then need to include it in the `PATH`
download_pandoc()

# Get Config from ini file
config = configparser.ConfigParser()
config.read('shaarli2hugo.ini')

BaseUrl = config.get('Url', 'BaseUrl')
TargetPath = config.get('Hugo', 'TargetPath')
Author = config.get('Hugo', 'author')

# Get Tags
Tags = config.items("Tags")
SearchTagsList = []
for key, tag in Tags:
    SearchTagsList.append(tag)

# Add a tag with actual year
YearTag = False
if config.has_option('Hugo', 'yeartag'):
    YearTag = True

# Create post as draft
Draft = False
if config.has_option('Hugo', 'draft'):
    Draft = True

# Get Topics
TopicsList = []
Topics = config.items("Topics")
for key, topic in Topics:
    TopicsList.append(topic)

# Hugo reads UTC time, so we need utc here
Now = datetime.datetime.utcnow()

# Get year, week number and day of week
year, week_num, day_of_week = Now.isocalendar()

# Date for hugo when the post is created
PostDateTime = Now.isoformat()

# Get Delta to week beginning (diff to monday) so on a wednesday, day_of_week=3,
margin = datetime.timedelta(days=day_of_week)
now = datetime.datetime.now(dtz.tzlocal())
beginningOfWeek = now-margin


def createSearchTagsString(TagsList):
    # Check SearchTagsList if multiple tags are in search
    Tags = ""
    if TagsList == []:
        # No limitation on tags, search over all posts
        url_part = "do=rss"
    elif len(TagsList) > 1:
        Tags = ""
        url_part = "do=rss&searchtags="
        for tag in TagsList:
            if Tags == "":
                delim = ""
            else:
                delim = "+"

            Tags = Tags + delim + tag
    else:
        Tags = TagsList[0]
        url_part = "do=rss&searchtags="

    return url_part + Tags


# Create URL to check posts
Url = BaseUrl + createSearchTagsString(SearchTagsList)
NewsFeed = feedparser.parse(Url)
PostsCount = len(NewsFeed.entries)

if PostsCount < 1:
    print("No new posts found")
    sys.exit()

if os.path.exists(TargetPath):
    fname = TargetPath + "/" + "linkdump_" + \
        str(week_num) + "_" + str(year) + ".adoc"
else:
    sys.exit()

if os.path.isfile(fname):
    print("File already created, stopping script.")
    sys.exit()
else:
    f = open(fname, "x")
    f.write("---\n")
    # Title of post with weeknumber
    f.write("title: Linkdump Week " + str(week_num) + " / " + str(year) + "\n")
    f.write("type: post\n")
    if Draft:
        f.write("draft: true\n")
    else:
        f.write("draft: false\n")
    f.write("author: " + Author + "\n")
    f.write("date: " + PostDateTime + "\n")
    f.write("tags:\n")
    for tag in SearchTagsList:
        f.write("  - " + tag + "\n")
    if YearTag:
        f.write("  - " + str(year) + "\n")
    f.write("topics:\n")
    for topic in TopicsList:
        f.write("  - " + topic + "\n")
    f.write("---\n")
    f.write(":icons: font\n:experimental:\n:under: pass:[_]\n:toc:\n\n")

    # Here follows the article
    for n in range(len(NewsFeed.entries)):
        EntryTitle = NewsFeed.entries[n].title
        EntryLink = NewsFeed.entries[n].link
        EntryCreated = NewsFeed.entries[n].updated
        # Only create an entry when the publishing date is after last sunday
        if dp.parse(EntryCreated) >= beginningOfWeek:
            EntryDescription = pypandoc.convert_text(NewsFeed.entries[n].summary.split(
                "<br />\n&#8212; <a")[0], 'asciidoc', format='html')
            f.write(EntryLink + "[" + EntryTitle + " &nbsp; icon:external-link[], window=_blank]:: " +
                    EntryDescription.replace('\n', ' ') + "\n\n")
    f.close()
